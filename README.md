# Internet Time Clock

A clock that keeps time in the Internet Time standard established by Swatch. From [their website](http://www.swatch.com/en_us/internet-time/):

>	### What is a Swatch .beat?
>	We have divided up the day into 1000 ".beats". So, one Swatch ".Beat" is equivalent to 1 Minute 26.4 Seconds.

>	### When did Internet Time start?
>	The BMT Meridian was inaugurated on October 23rd, 1998, in the presence of Nicholas Negroponte, founder and
>	director of the media laboratory at the Massachusetts Institute of Technology.

>	### Where is the Internet Time meridian?
>	The BMT Meridian was inaugurated on October 23rd, 1998, in the presence of Nicholas Negroponte, founder and
>	director of the media laboratory at the Massachusetts Institute of Technology.

>	### Why use Internet Time?
>	Internet Time exists so that we do not have to think about timezones. For example, if a New York web-supporter 
>	makes a date for a chat with a cyber friend in Rome, they can simply agree to meet at an "@ time" - because
>	internet time is the same all over the world.

Why did I make a clock for it? Well, the only place you could find the time was on Swatch's website, in a tiny corner.
I wanted to change that, since I think the concept's pretty sweet. It's that simple.
